#! /bin/bash
set -e

while getopts input:archive:projectName: option
do
case "${option}"
in
input) SRCROOT=${OPTARG};;
archive) ARCHIVE=${OPTARG};;
projectName) INPUT_PROJECT_NAME=${OPTARG};;
esac
done

RED_COLOR='\033[0;31m'
LGRAY_COLOR='\033[0;37m'
GREEN_COLOR='\033[1;32m'
WHITE_COLOR='\033[1;37m'
NORMAL_COLOR='\033[0m'
CLRLINE=$(tput el)
CHECK_MARK="\033[0;32m\xE2\x9C\x94\033[0m"
X_MARK="⚠️ "

echo -e "${GREEN_COLOR}"
echo "     __________  __________  ___    ____  ____  ____  _____ ";
echo "    / ____/ __ \/ ____/ __ \/   |  / __ \/ __ \/ __ \/ ___/ ";
echo "   / /   / / / / /   / / / / /| | / /_/ / / / / / / /\__ \  ";
echo "  / /___/ /_/ / /___/ /_/ / ___ |/ ____/ /_/ / /_/ /___/ /  ";
echo "  \____/\____/\____/\____/_/  |_/_/    \____/_____//____/   ";
echo "                                                            ";
echo -e "${WHITE_COLOR}"

CARTHAGE_BUILDS_IOS_PATH='$(SRCROOT)/Carthage/Build/iOS/'
CARTHAGE_FRAMEWORK_OUTPUTS_PATH='$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/'

ppid () { ps -p ${1:-$$} -o ppid=; }
XCODE_BUILD_PID=$(ppid $$)
CARTHAGE_PID=$(ppid $XCODE_BUILD_PID)
CARTHAGE_PARENT_PID=$(ppid $CARTHAGE_PID)
CARTHAGE_ROOT_PID=$(ppid $CARTHAGE_PARENT_PID)
CARTHAGE_CARTFILE_PATH=$(lsof -p $CARTHAGE_ROOT_PID | awk '$4=="cwd" {print $9}')
if [ -z "$SRCROOT" ]; then
    if [ "$ARCHIVE" == "YES" ]; then
        SRCROOT="./";
    else
        if [ -z "$CARTHAGE_CARTFILE_PATH" ]; then
            SRCROOT=$CARTHAGE_CARTFILE_PATH/Carthage/Checkouts/$INPUT_PROJECT_NAME;
        else
            SRCROOT="./";
        fi
    fi
fi
echo "SRCROOT is set to ${SRCROOT}";

env -i LANG="en_US.UTF-8" HOME="$HOME" SRCROOT="$SRCROOT" bash -l -c "cd $SRCROOT && pod deintegrate && pod install"

XCODE_PODS_PATH=$SRCROOT/Pods/Pods.xcodeproj
TARGETS_OUTPUT_STR=$(xcodebuild -project $XCODE_PODS_PATH -list)
TARGETS_STR=$(echo $TARGETS_OUTPUT_STR | sed -e 's/.*Targets: \(.*\) Build Configurations:.*/\1/')
TARGETS_EXTRACT=' ' read -r -a TARGETS <<< "$TARGETS_STR"

OBJ_BUILD_DIR=$SRCROOT/build/
RELEASE_OBJ_PATH=$OBJ_BUILD_DIR/Release-iphoneos
FRAMEWORK_OBJ_PATH=$OBJ_BUILD_DIR/UninstalledProducts/iphoneos

CARTHAGE_CWD=$CARTHAGE_CARTFILE_PATH/Carthage/Build/iOS
CARTHAGE_BUILD_FRAMEWORKS=$OBJ_BUILD_DIR/Frameworks

rm -Rf $OBJ_BUILD_DIR 2>/dev/null || :
rm -Rf $CARTHAGE_BUILD_FRAMEWORKS 2>/dev/null || :
mkdir -p $CARTHAGE_BUILD_FRAMEWORKS && chmod 755 $CARTHAGE_BUILD_FRAMEWORKS 2>/dev/null || :

echo -e "${GREEN_COLOR}\nBuilding frameworks...${WHITE_COLOR}"
for (( i=0; i<${#TARGETS[@]}; i++ ))
do
    TARGET=${TARGETS[$i]}
    if [[ $TARGET == *"Pods-"* ]]; then
        continue
    fi
    echo -ne "${LGRAY_COLOR}\t> ${TARGET}: ${WHITE_COLOR}"
    if env -i HOME="$HOME" bash -l -c "/usr/bin/xcrun xcodebuild -project ${XCODE_PODS_PATH} -target $TARGET -configuration Release -sdk iphoneos ONLY_ACTIVE_ARCH=NO CODE_SIGNING_REQUIRED=YES CARTHAGE=NO archive SKIP_INSTALL=YES GCC_INSTRUMENT_PROGRAM_FLOW_ARCS=NO CLANG_ENABLE_CODE_COVERAGE=NO STRIP_INSTALLED_PRODUCT=NO > /dev/null 2>&1" ; then
        echo -e "${GREEN_COLOR} ${CHECK_MARK} ${TARGET} done${WHITE_COLOR}"
    else
        echo -e "${RED_COLOR} ${X_MARK} ${TARGET} error${WHITE_COLOR}"
    fi
done

echo -e "${GREEN_COLOR}\nCopying signatures...${WHITE_COLOR}"
for SIGNATURE_PATH in $RELEASE_OBJ_PATH/* ; do
    FRAMEWORK_NAME="$(basename -- $SIGNATURE_PATH)"
    if [ -d ${SIGNATURE_PATH} ]; then
        if [ -f "${SIGNATURE_PATH}/*.framework" ] ; then
            rm ${SIGNATURE_PATH}/*.framework
        fi
        if cp -a ${SIGNATURE_PATH}/* $CARTHAGE_BUILD_FRAMEWORKS > /dev/null 2>&1; then
            echo -e "${GREEN_COLOR}\t${CHECK_MARK} ${FRAMEWORK_NAME} signatures copied${WHITE_COLOR}"
        else
            echo -e "${RED_COLOR}\t${X_MARK} ${FRAMEWORK_NAME} has missing signatures${WHITE_COLOR}"
        fi
    fi
done

echo -e "${GREEN_COLOR}\nCopying frameworks...${WHITE_COLOR}"
for FRAMEWORK_PATH in $FRAMEWORK_OBJ_PATH/*.framework ; do
    FRAMEWORK_NAME="$(basename -- $FRAMEWORK_PATH)"
    if [ -d ${FRAMEWORK_PATH} ]; then
        if cp -Ra $FRAMEWORK_PATH/* $CARTHAGE_BUILD_FRAMEWORKS > /dev/null 2>&1; then
            echo -e "${GREEN_COLOR}\t${CHECK_MARK} ${FRAMEWORK_NAME} framework body copied${WHITE_COLOR}"
        else
            echo -e "${RED_COLOR}\t${X_MARK} ${FRAMEWORK_NAME} has missing framework body${WHITE_COLOR}"
        fi
    fi
done

echo -e "${GREEN_COLOR}\nCopying bundles...${WHITE_COLOR}"
for BUNDLE_PATH in $FRAMEWORK_OBJ_PATH/*.framework ; do
    BUNDLE_NAME="$(basename -- $BUNDLE_PATH)"
    if [ -d ${BUNDLE_PATH} ]; then
        if cp -Ra $BUNDLE_PATH/* $CARTHAGE_BUILD_FRAMEWORKS > /dev/null 2>&1; then
            echo -e "${GREEN_COLOR}\t${CHECK_MARK} ${BUNDLE_NAME} bundle copied${WHITE_COLOR}"
        else
            echo -e "${RED_COLOR}\t${X_MARK} ${BUNDLE_NAME} has missing bundle files${WHITE_COLOR}"
        fi
    fi
done

echo -e "${GREEN_COLOR}\nYou can use following paths in your INPUT xcfilelist:${WHITE_COLOR}"
for FRAMEWORK_PATH in $FRAMEWORK_OBJ_PATH/*.framework ; do
    FRAMEWORK_NAME="$(basename -- $FRAMEWORK_PATH)"
    if [ -d ${FRAMEWORK_PATH} ]; then
        echo -e "${WHITE_COLOR}\t${CARTHAGE_BUILDS_IOS_PATH}${FRAMEWORK_NAME}${NORMAL_COLOR}"
    fi
done

echo -e "${GREEN_COLOR}\nYou can use following paths in your OUTPUT xcfilelist:${WHITE_COLOR}"
for FRAMEWORK_PATH in $FRAMEWORK_OBJ_PATH/*.framework ; do
    FRAMEWORK_NAME="$(basename -- $FRAMEWORK_PATH)"
    if [ -d ${FRAMEWORK_PATH} ]; then
        echo -e "${GREEN_COLOR}\t${CARTHAGE_FRAMEWORK_OUTPUTS_PATH}${FRAMEWORK_NAME}${NORMAL_COLOR}"
    fi
done

mkdir -p $CARTHAGE_CWD && chmod 755 $CARTHAGE_CWD
if cp -a $CARTHAGE_BUILD_FRAMEWORKS/* $CARTHAGE_CWD > /dev/null 2>&1; then
    echo -e "${GREEN_COLOR}\t> All ready pods are now located in ${CARTHAGE_CWD}${WHITE_COLOR}\n"
else
    echo -e "${RED_COLOR}\t> Your pods can't be copied${NORMAL_COLOR}"
fi
